/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.products.RestControllers;

import com.products.bean.ProductBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.products.Repositories.ProductsRepository;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author mamuk
 */
@RestController
@RequestMapping("/v1/products")
public class ProductController {

    @RequestMapping(value = {"/add-product"}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> addProduct(@RequestBody ProductBean request) {
        ResponseEntity<Void> build = null;
        System.out.println("" + request.getImage());
        HashMap response = new HashMap();
        int addProducts = ProductsRepository.getInstance().addProducts(request);
        if (addProducts == 1) {
            build.status(HttpStatus.CREATED).build();
            return build;
        } else {
            build.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return build;
    }

    @RequestMapping(value = {"/get-product"}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map>> getProduct(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "color", required = false) String color,
            @RequestParam(value = "category", required = false) String category) {
//        ResponseEntity<List<Map>> build = null;
        HttpHeaders responseHeaders = new HttpHeaders();
        List<Map> products = ProductsRepository.getInstance().getProducts(name, color, category);
//        build.(products, responseHeaders, HttpStatus.OK);
        ResponseEntity<List<Map>> responseEntity = new ResponseEntity<>(products,
                HttpStatus.OK);
        return responseEntity;
    }
}
