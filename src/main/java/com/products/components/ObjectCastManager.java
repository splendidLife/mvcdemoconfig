/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.products.components;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mamuk
 */
public class ObjectCastManager {

    private static ObjectCastManager instance;

    private ObjectCastManager() {
    }

    public static ObjectCastManager getInstance() {
        if (instance == null) {
            instance = new ObjectCastManager();
        }
        return instance;
    }

  

    public List<Map> resultSetCasting(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        String[] columnArray = new String[columnCount];
        for (int i = 1; i <= columnCount; i++) {
            String columnName = metaData.getColumnLabel(i);
            columnArray[i - 1] = columnName;
        }
        List<Map> mapList = null;
        Map map = null;
        while (rs.next()) {
            if (mapList == null) {
                mapList = new LinkedList<>();
            }
            map = new HashMap();
            for (String columnName : columnArray) {
                String columValue = rs.getString(columnName);
                if (columValue != null) {
                    columValue = columValue.replaceAll("\"", "'");
                }
                map.put(columnName,columValue);
            }
            mapList.add(map);
        }
        return mapList;
    }

  
}