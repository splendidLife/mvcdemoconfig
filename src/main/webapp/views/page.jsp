<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            * {box-sizing: border-box}

            /* Add padding to containers */
            .container {
                padding: 16px;
            }

            /* Full-width input fields */
            input[type=text] , input[type=number] , input[type=file] {
                width: 100%;
                padding: 15px;
                margin: 5px 0 22px 0;
                display: inline-block;
                border: none;
                background: #f1f1f1;
            }

            /* Full-width input fields */
            input[type=color] {
                width: 100%;
                padding: -15px;
                margin: 5px 0 22px 0;
                display: inline-block;
                border: none;
                background: #f1f1f1;
            }

            input[type=text]:focus, input[type=color]:focus {
                background-color: #ddd;
                outline: none;
            }

            /* Overwrite default styles of hr */
            hr {
                border: 1px solid #f1f1f1;
                margin-bottom: 25px;
            }

            /* Set a style for the submit/register button */
            .registerbtn {
                background-color: #4CAF50;
                color: white;
                padding: 16px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
                opacity: 0.9;
            }

            .registerbtn:hover {
                opacity:1;
            }

            /* Add a blue text color to links */
            a {
                color: dodgerblue;
            }

            /* Set a grey background color and center the text of the "sign in" section */
            .signin {
                background-color: #f1f1f1;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Add Product</h1>
            <p>Please fill in this form to Save an product.</p>
            <hr>

            <label for="name"><b>Name</b></label>
            <input type="text" placeholder="Enter Name" name="name" id="name" required>

            <label for="image"><b>Image</b></label>
            <input type="file"  name="image" id="image" value="Upload a Image" required>

            <label for="price"><b>Price</b></label>
            <input type="number" placeholder="Enter Price" name="price" min="0" id="price" required>

            <label for="color"><b>Color</b></label>
            <input type="color" placeholder="Select Color" value="Select a color" name="color" id="color" required>

            <label for="category"><b>Categories</b></label>
            <input type="text" placeholder="Enter Category" name="category" id="category" required>

            <hr>

            <button class="registerbtn" id ="submit">Add Product</button>

            <h1>All Products</h1>
            <h3>By Name</h3>
            <input type="text" placeholder="Enter Name"  id="nameSearch" >
            <h3>By color</h3>
            <input type="color" placeholder="Enter Name"  id="colorSearch" >
            <h3>By Category</h3>
            <input type="text" placeholder="Enter Category"  id="categorySearch" >

            <button class="registerbtn" id ="getProduct">Get Products</button>
            <div id="allProduct"></div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>

            let imageBase64 = "";
            function readFile() {

                if (this.files && this.files[0]) {

                    var FR = new FileReader();

                    FR.addEventListener("load", function (e) {
//                        document.getElementById("img").src = e.target.result;
                        imageBase64 = e.target.result;
                    });

                    FR.readAsDataURL(this.files[0]);
                }

            }

            document.getElementById("image").addEventListener("change", readFile);
            $(document).ready(function () {
                $("#submit").click(function () {
                    var name = $("#name").val();
                    var color = $("#color").val();
                    var price = $("#price").val();
//                    var imageName = $("#image").val();
                    var image = imageBase64;
                    var category = $("#category").val();

                    var obj = {
                        name: name,
                        color: color,
                        price: price,
                        image: image,
                        category: category
                    };

                    if (name === "" || color === "" || price === "" || category === "") {
                        alert("Please Fill all Details");
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: './v1/products/add-product',
                            headers: {"Content-Type": 'application/json'},
                            //                            dataType: 'json',
                            data: JSON.stringify(obj),
                            success: function (data, textStatus, jqXHR) {
                                //                                var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
//                            swal({
//                                title: "Proceed",
//                                text: "Product has Created successfully.",
//                                type: "success",
//                                timer: 2500,
//                                showConfirmButton: true
//                            });
//                            location.reload(true);
                            },
                            error: function (jqXHR, textStatus, errorThrown) {


                            }
                        });
                    }

                });

                $("#getProduct").click(function () {
                    var name = $("#nameSearch").val();
                    var color = $("#colorSearch").val();
                    var category = $("#categorySearch").val();

                    $.ajax({
                        type: 'GET',
                        url: './v1/products/get-product?name=' + name + '&color=' + color + '&category=' + category,
//                        headers: {"Content-Type": 'application/json'},
                        //                            dataType: 'json',
                        success: function (data, textStatus, jqXHR) {
                            var e = document.getElementById("allProduct");

                            //e.firstElementChild can be used. 
                            var child = e.lastElementChild;
                            while (child) {
                                e.removeChild(child);
                                child = e.lastElementChild;
                            }
                            console.log(data);
//                            var obj = JSON.parse(data);
//                            var obj = JSON.parse(data.replace(/[\n\t\r]/g, ' '));
                            for (var i = 0; i < data.length; i++) {
                                var pName = document.createElement("p");
                                var pImg = document.createElement("img");
                                pImg.src = data[i].image;
                                pName.innerHTML = "Name :" + data[i].name + "   Color :" + data[i].color + " Price:" + data[i].price + " Category:" + data[i].category;
                                document.getElementById("allProduct").appendChild(pName);
                                document.getElementById("allProduct").appendChild(pImg);
                            }

//                            swal({
//                                title: "Proceed",
//                                text: "Product has Created successfully.",
//                                type: "success",
//                                timer: 2500,
//                                showConfirmButton: true
//                            });
//                            location.reload(true);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(jqXHR);

                        }
                    });
                });
            });
        </script>
    </body>
</html>
