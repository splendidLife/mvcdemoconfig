/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.products.config;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
//@EnableWebMvc
//@ComponentScan(basePackages = { "com.products.*" })
public class WebConfig extends WebMvcConfigurerAdapter {
 
// @Autowired
// DataSource dataSource;
// 
// @Bean
// public NamedParameterJdbcTemplate geNamedParameterJdbcTemplate(){
//  return new NamedParameterJdbcTemplate(dataSource);
// }
 
 
 
 @Bean(name = "dataSource")
    public BasicDataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://remotemysql.com:3306/fU9y9jGoHp");
        dataSource.setUsername("fU9y9jGoHp");
        dataSource.setPassword("2PlKaiJMzL");
        dataSource.setInitialSize(10);
        dataSource.setMaxActive(25);
        dataSource.setMaxIdle(1);
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("SELECT 1");
        return dataSource;
    }
 
// @Override
// public void addResourceHandlers(ResourceHandlerRegistry registry) {
//  registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
// }
 
// @Bean
// public InternalResourceViewResolver viewResolver(){
//  InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//  viewResolver.setViewClass(JstlView.class);
//  viewResolver.setPrefix("/WEB-INF/views/");
//  viewResolver.setSuffix(".jsp");
//  
//  return viewResolver;
// }
}
