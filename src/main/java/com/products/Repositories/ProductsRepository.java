/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.products.Repositories;

import com.products.bean.ProductBean;
import com.products.components.ObjectCastManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author mamuk
 */
public class ProductsRepository {

    private static ProductsRepository instance;

    public static ProductsRepository getInstance() {
        if (instance == null) {
            instance = new ProductsRepository();
        }
        return instance;
    }

    public int addProducts(ProductBean bean) {

        String sql = "INSERT INTO `products`\n"
                + "(`name`,`price`,`color`,`category`,`image`,`imageName`,`active`,`createdTs`)\n"
                + "VALUES (?,?,?,?,?,?,'Y',?);";
        String name = bean.getImage().split(";base64")[0].split("/")[1];
        String imageName = bean.getName().concat("_").concat(bean.getCategory()).concat("_").concat(String.valueOf(new Date().getTime())).concat(".").concat(name);
        System.out.println("imageName------" + imageName);
        Connection con = null;
        try {
            con = DBConnection.getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, bean.getName());
            ps.setDouble(2, bean.getPrice());
            ps.setString(3, bean.getColor());
            ps.setString(4, bean.getCategory());
            ps.setString(5, bean.getImage());
            ps.setString(6, imageName);
            ps.setTimestamp(7, new Timestamp(new java.util.Date().getTime()));
            int i = ps.executeUpdate();
            return i;
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductsRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;

    }

    public List<Map> getProducts(String name, String color, String category) {
        String sql = "SELECT * FROM `products` where active = 'Y'";
        if (name != null && name != "") {
            sql += "or name='" + name + "'";
        } else if (color != null && color != "") {
            sql += "or color='" + color + "'";
        } else if (color != null && category != "") {
            sql += "or category='" + category + "'";
        }

        Connection con = null;

        try {

            con = DBConnection.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            List<Map> productList = ObjectCastManager.getInstance().resultSetCasting(rs);
            return productList;
        } catch (SQLException ex) {
            Logger.getLogger(ProductsRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductsRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
}
